https = require 'https'
Bucket = require './bucket'

class Api
  constructor: (appID, accessToken, options = {}) ->
    throw new TypeError 'Missing appID' unless appID?
    throw new TypeError 'Missing accessToken' unless accessToken?

    @_appID = appID
    @_accessToken = accessToken

    @_apiVersion = options.apiVersion or '1'
    @_endpoint = options.endpoint or 'api.simperium.com'

  bucket: (bucketName) ->
    throw new TypeError 'Missing bucketName' unless bucketName?

    bucket = new Bucket @_appID,
      @_accessToken,
      bucketName,
      @_endpoint,
      @_apiVersion

    return bucket

module.exports = Api

