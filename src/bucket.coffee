http = require 'http'
request = require 'request'
uuid = require 'node-uuid'

class Bucket
  constructor: (appID, accessToken, bucketName, endpoint, apiVersion) ->
    @_appID = appID
    @_accessToken = accessToken
    @_bucketName = bucketName
    @_endpoint = endpoint
    @_apiVersion = apiVersion

    @_basePath = [@_apiVersion, @_appID, @_bucketName].join '/'

  _request: (options = {}, cb = ->) ->
    path = options.path or ''
    method = options.method or 'GET'
    data = options.data or null

    headers =
      'X-Simperium-Token': @_accessToken

    reqOptions =
      url: "https://#{@_endpoint}/#{@_basePath}#{path}"
      method: method
      headers: headers

    if data?
      reqOptions.json = data
      delete reqOptions.data if reqOptions.data?

    # make the request
    request reqOptions, (err, res, body) ->
      if err?
        return console.log err

      # we are always talking in JSON, so make sure it is if we get a
      # 'successful' reply
      if body? and res.statusCode is 200
        unless typeof body is 'object'
          try
            body = JSON.parse body
          catch ex
            error = new Error "Invalid JSON"
            return cb error, res, body

      # pass along the information to the callback
      return cb err, res, body

  get: (itemID, options = {}, cb = ->) ->
    throw new TypeError 'Missing itemID' unless itemID?

    if arguments.length < 3 && (typeof options is 'function')
      cb = options
      options = {}

    path = "/i/#{itemID}"

    if options.version?
      path += "/v/#{options.version}"

    @_request { path: path, method: 'GET' }, (err, res, body) ->
      if err?
        return cb err, null

      switch res.statusCode
        when 200
          return cb null, body
        else
          msg = "#{res.statusCode} #{http.STATUS_CODES[res.statusCode]}"
          error = new Error msg
          return cb error, null

  set: (itemID, data, options = {}, cb = ->) ->
    throw new TypeError 'Missing itemID' unless itemID?
    throw new TypeError 'Missing data' unless data?

    if arguments.length < 4 && (typeof options is 'function')
      cb = options
      options = {}

    path = "/i/#{itemID}"

    if options.version?
      path += "/v/#{options.version}"

    path += "?response=1"

    @_request { path: path, method: 'POST', data: data }, (err, res, body) ->
      if err?
        return cb err

      switch res.statusCode
        when 200
          return cb null, body
        else
          msg = "#{res.statusCode} #{http.STATUS_CODES[res.statusCode]}"
          error = new Error msg
          return cb error, null

  new: (data, cb = ->) ->
    throw new TypeError 'Missing data' unless data?
    unless data.id
      # make UUID compact
      data.id = uuid.v4().replace /-/g, ''

    @_request { path: "/i/#{data.id}?response=1", method: 'POST', data: data },
      (err, res, body) ->
        if err?
          return cb err, null

        switch res.statusCode
          when 200
            return cb null, body.id, body
          else
            msg = "#{res.statusCode} #{http.STATUS_CODES[res.statusCode]}"
            error = new Error msg
            return cb error, null, null

  delete: (itemID, options = {}, cb = ->) ->
    throw new TypeError 'Missing itemID' unless itemID?

    if arguments.length < 3 && (typeof options is 'function')
      cb = options
      options = {}

    path = "/i/#{itemID}"

    if options.version?
      path += "/v/#{options.version}"

    @_request { path: path, method: 'DELETE' }, (err, res, body) ->
      if err?
        return cb err

      switch res.statusCode
        when 200
          return cb null
        else
          msg = "#{res.statusCode} #{http.STATUS_CODES[res.statusCode]}"
          error = new Error msg
          return cb error

  all: (options = {}, cb = ->) ->
    if arguments.length < 2 and (typeof options is 'function')
      # we were only passed a callback
      cb = options
      options = {}

    path = "/all"

    if options.cv?
      path += "?cv=#{options.cv}"

    if options.data?
      path += "?data=#{options.data}"

    if options.username?
      path += "?username=#{options.username}"

    @_request { path: path, method: 'GET' }, (err, res, body) ->
      if err?
        return cb err, null

      switch res.statusCode
        when 200
          return cb null, body
        else
          msg = "#{res.statusCode} #{http.STATUS_CODES[res.statusCode]}"
          error = new Error msg
          return cb error, null

module.exports = Bucket

