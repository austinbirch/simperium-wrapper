http = require 'http'
request = require 'request'

class Auth
  constructor: (appID, apiKey, options = {}) ->
    throw new TypeError "Missing appID" unless appID?
    throw new TypeError "Missing apiKey" unless apiKey?

    @_appID = appID
    @_apiKey = apiKey

    @_endpoint = options.endpoint or 'auth.simperium.com'
    @_apiVersion = options.apiVersion or '1'

  authorize: (username, password, cb) ->
    throw new TypeError "Missing username" unless username?
    throw new TypeError "Missing password" unless password?

    formData =
      username: username
      password: password

    headers =
      'X-Simperium-API-Key': @_apiKey

    reqOptions =
      uri: "https://#{@_endpoint}/#{@_apiVersion}/#{@_appID}/authorize/"
      method: 'POST'
      form: formData
      headers: headers

    request reqOptions, (err, res, body) ->
      if err?
        error = new Error err
        return cb error, null

      switch res.statusCode
        when 200
          try
            data = JSON.parse body
          catch ex
            error = new Error "Invalid JSON"
            return cb error, null
          return cb null, data.access_token
        else
          msg = "#{res.statusCode} #{http.STATUS_CODES[res.statusCode]}"
          error = new Error msg
          return cb error, null

module.exports = Auth

