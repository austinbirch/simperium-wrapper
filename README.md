# simperium-wrapper

A [node.js](http://nodejs.org) module, written in
[CoffeeScript](http://coffeescript.org), that wraps the
[Simperium](https://simperium.com) HTTP API.

As a work in progress, I have only implemented the parts of the API I have
needed so far. However, it should be easy enough to implement the rest of the
API using this.

### WARNING

At the moment simperium-wrapper does not include support for versions in
Simperium. This makes it __quite unstable__, and __not ready for use 
in a production environment__.

The [usage](#usage) documentation is fairly complete, so anything that isn’t
included in that has not yet been implemented.

## Installation

Install [npm](http://npmjs.com) first, if you do not already have it. Then run
```npm install simperium-wrapper```, or add it as a dependency to
your project’s ```package.json``` file.

## Usage

The usage instructions below use CoffeeScript, but you can use JavaScript if
you like.

Require simperium-wrapper to get access to the Auth, Api, and Bucket classes.

```CoffeeScript
Simperium = require 'simperium-wrapper'
```

### Auth

#### constructor ```(appID, apiKey, options)```

Creates a new Auth instance for authenticating users.

__arguments:__

```appID``` (string) – Your Simperium App ID.  
```apiKey``` (string) – Your Simperium App Api Key.  
```options``` – (object, optional):  
&nbsp;&nbsp;&nbsp;&nbsp; - ```endpoint``` (string) – The API endpoint. 
  Defaults to 'auth.simperium.com'.  
&nbsp;&nbsp;&nbsp;&nbsp; - ```apiVersion``` (string) – The API version. 
  Defaults to '1'.  

__example:__

```CoffeeScript
auth = new Simperium.Auth 'appID', 'apiKey'
```

#### ```authorize (username, password, cb)```

Authenticates a user.

__arguments:__

```username``` (string) – The User’s username (currently must be an email).  
```password``` (string) – The User’s password.  
```cb``` (function) - The callback. Data passed in is:  
&nbsp;&nbsp;&nbsp;&nbsp; - ```err``` (object) – Describes any error that has 
  occurred, otherwise null.  
&nbsp;&nbsp;&nbsp;&nbsp; - ```accessToken``` (string) – Token for accessing the
  API.  

__example:__

```CoffeeScript
auth.authorize 'username', 'password', (err, accessToken) ->
  if err?
    # handle error

  console.log accessToken
```

### Api

#### constructor ```(appID, accessToken, options)```

Creates a new Api instance for accessing buckets.

__arguments:__

```appID``` (string) – Your Simperium App ID.  
```accessToken``` (string) – Token generated from Auth.authorize call.  
```options``` – (object, optional):  
&nbsp;&nbsp;&nbsp;&nbsp; - ```endpoint``` (string) – The API endpoint. 
  Defaults to 'auth.simperium.com'.  
&nbsp;&nbsp;&nbsp;&nbsp; - ```apiVersion``` (string) – The API version. 
  Defaults to '1'.  

__example:__

```CoffeeScript
api = new Simperium.Api 'appID', 'accessToken'
```

#### ```bucket (bucketName)```

Creates a new Bucket instance for accessing data in the bucket.

__arguments:__

```bucketName``` (string) – The bucket name.

__example:__

```CoffeeScript
bucket = api.bucket 'reports'
```

### Bucket

#### constructor ```(appID, accessToken, bucketName, endpoint, apiVersion)```

You should get a Bucket instance indirectly using ```api.bucket()```.

#### ```get (itemID, options, cb)```

Gets an item from the bucket.

__arguments:__

```itemID``` (string) – The ID of the object in the bucket.  
```options``` (object, optional):  
&nbsp;&nbsp;&nbsp;&nbsp; - ```version``` (string) – The version of the object.  
```cb``` (function) – The callback. Data passed in is:  
&nbsp;&nbsp;&nbsp;&nbsp; - ```err``` (object) – Describes any error that has 
  occurred, otherwise null.  
&nbsp;&nbsp;&nbsp;&nbsp; - ```item``` (object) – The item.  

__example:__

```CoffeeScript
bucket.get '1', options, (err, item) ->
  if err?
    # handle error
    ...

  console.log item
```

#### ```set (itemID, data, options, cb)```

Sets a value on the object. This merges the keys you are setting with the keys
that have already been set.

__arguments:__

```itemID``` (string) – The ID of the object in the bucket.  
```data``` (object) – The data to set.  
```options``` (object, optional):  
&nbsp;&nbsp;&nbsp;&nbsp; - ```version``` (string) – The version of the
  object that you want to alter.  
```cb``` (function) – The callback. Data passed in is:  
&nbsp;&nbsp;&nbsp;&nbsp; - ```err``` (object) – Describes any error that has
  occurred, otherwise null.  
&nbsp;&nbsp;&nbsp;&nbsp; - ```item``` (object) – The item.  

__example:__

```CoffeeScript
bucket.set '1', {name: 'Mary'}, options, (err, item) ->
  if err?
    # handle error
    ...

  console.log item
```

#### ```new (data, cb)```

Creates a new item in the bucket. If the data object has an 'id' property, then
that is used as the item’s id in the bucket.

__arguments:__

```data``` (object) – The new item’s data.  
```cb``` (function) – The callback. Data passed in is:  
```err``` (object) – Describes any error that has occurred, otherwise null.  
&nbsp;&nbsp;&nbsp;&nbsp; - ```id``` (string) – The new item’s id.  
&nbsp;&nbsp;&nbsp;&nbsp; - ```item``` (object) – The item.  

__example:__

```CoffeeScript
bucket.new {name:'Mary'}, (err, item) ->
  if err?
    # handle error
    ...

  console.log item.id
```

#### ```delete (itemID, options, cb)```

Deletes an item from the bucket.

__arguments:__

```itemID``` (string) – The ID of the object in the bucket.  
```options``` (object, optional):  
&nbsp;&nbsp;&nbsp;&nbsp; - ```version``` (string) – The version of the object.  
```cb``` (function) – The callback. Data passed in is:  
&nbsp;&nbsp;&nbsp;&nbsp; - ```err``` (object) – Describes any error that has
  occurred, otherwise null.  

__example:__

```CoffeeScript
bucket.delete '1', options, (err) ->
  if err?
    # handle error
    ...

  console.log 'item deleted!'
```

#### ```all (options, cb)```

Polls Simperium for all changes being made to this bucket, regardless of who
is making the changes. To use this function, you must have used the admin key
(instead of the api key), when initializing the Api.

__arguments:__

```options``` (object, optional):  
&nbsp;&nbsp;&nbsp;&nbsp; - ```username``` (boolean) – If true, the username for
  who created the change
    will be returned.  
&nbsp;&nbsp;&nbsp;&nbsp; - ```data``` (boolean) – If true, the latest version’s
  data will also be
    returned.  
&nbsp;&nbsp;&nbsp;&nbsp; - ```cv``` (string) – Listen for changes that happened
  _after_ this version.  
```cb``` (function) – The callback. Data passed in is:  
&nbsp;&nbsp;&nbsp;&nbsp; - ```err``` (object) – Describes any error that has
  occurred, otherwise null.  
&nbsp;&nbsp;&nbsp;&nbsp; - ```changes``` (object) – The changes.  

__example:__

```CoffeeScript
bucket.all options, (err, changes) ->
  if err?
    # handle error
    ...

  console.log change for change in changes
```

## Develop / Contribute

First, install [npm](http://npmjs.com) if you do not already have it.

Then, clone this repository and run (from within the directory):

```Shell
npm install && npm link
```

I recommend installing coffee-script globally via
```npm install coffee-script``` if you are going to use the ```Cakefile```
to build/watch the code.

Pull requests are more than welcome :)

### Cake

There is a (rather messy) ```Cakefile``` included that can help automate the
build and test tasks.

- ```cake build``` builds the CoffeeScript files in ```src/``` into ```lib/```.
- ```cake test``` runs the tests from ```test/```.
- ```cake watch``` runs both the ```build``` and ```test``` tasks whenever a 
  change is detected in ```src/``` or ```test/```.

## License

simperium-wrapper is released under the MIT License
(see ```LICENSE``` for details).

