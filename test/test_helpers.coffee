# require in each test file to access the assert library,
# and the testConfig (exported as tc also, for shorthand)
testConfig =
  appID: 'APPID'
  apiKey: 'APIKEY'
  apiVersion: '1'
  username: 'test@test.com'
  password: 'testtest'
  bucketName: 'testbucket'

TestHelpers =
  testConfig: testConfig
  tc: testConfig

  assert: require('chai').assert

module.exports = TestHelpers

