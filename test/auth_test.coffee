{tc, assert} = require './test_helpers'

Simperium = require '../lib/index'
nock = require 'nock'
querystring = require 'querystring'

suite "Auth", ->
  test "requiring the module defines Simperium.Auth", ->
    assert.isDefined Simperium.Auth

  suite "constructor()", ->
    test "calling without an appID throws an error", ->
      fn = =>
        new Simperium.Auth()
      assert.throws fn, TypeError, /Missing appID/

    test "calling without an apiKey throws an error", ->
      fn = =>
        new Simperium.Auth tc.appID
      assert.throws fn, TypeError, /Missing apiKey/

    test "passing an options object with endpoint sets the endpoint", ->
      auth = new Simperium.Auth tc.appID, tc.apiKey, endpoint: 'hello'
      assert.equal auth._endpoint, 'hello'

    test "passing an options object with apiVersion sets the apiVersion", ->
      auth = new Simperium.Auth tc.appID, tc.apiKey, apiVersion: '2'
      assert.equal auth._apiVersion, '2'

  suite "authorize()", ->
    setup ->
      @simperium = new Simperium.Auth tc.appID, tc.apiKey

    test "calling without a username throws an error", ->
      fn = =>
        @simperium.authorize()
      assert.throws fn, TypeError, /Missing username/

    test "calling without a password throws an error", ->
      fn = =>
        @simperium.authorize tc.username
      assert.throws fn, TypeError, /Missing password/

    test "returns an access token on successful auth", (done) ->
      data =
        username: tc.username
        password: tc.password

      scope = nock('https://auth.simperium.com')
        .post("/#{tc.apiVersion}/#{tc.appID}/authorize/",
          querystring.stringify(data))
        .reply(200, { access_token: 'testtoken' })

      cb = (err, accessToken) =>
        assert.isNull err
        assert.equal scope.isDone(), true
        assert.equal accessToken, 'testtoken'
        done()
      @simperium.authorize tc.username, tc.password, cb

    test "returns an Error if authorization fails", (done) ->
      incorrectPassword = 'pass'

      data =
        username: tc.username
        password: incorrectPassword

      scope = nock('https://auth.simperium.com')
        .post("/#{tc.apiVersion}/#{tc.appID}/authorize/",
          querystring.stringify(data))
        .reply(401, { access_token: 'testtoken' })

      cb = (err, accessToken) =>
        assert.instanceOf err, Error
        assert.isNull accessToken
        assert.match err.message, /401 Unauthorized/
        assert.equal scope.isDone(), true
        done()
      @simperium.authorize tc.username, incorrectPassword, cb

