{tc, assert} = require './test_helpers'

nock = require 'nock'
Simperium = require '../lib/index'

suite "Api", ->
  test "requiring the module defines Simperium.Api", ->
    assert.isDefined Simperium.Api

  suite "constructor()", ->
    test "calling without an appID throws an error", ->
      fn = =>
        new Simperium.Api()
      assert.throws fn, TypeError, /Missing appID/

    test "calling without an accessToken throws an error", ->
      fn = =>
        new Simperium.Api tc.appID
      assert.throws fn, TypeError, /Missing accessToken/

    test "passing an options object with endpoint sets the endpoint", ->
      api = new Simperium.Api tc.appID, tc.apiKey, endpoint: 'hello'
      assert.equal api._endpoint, 'hello'

    test "passing an options object with apiVersion sets the apiVersion", ->
      api = new Simperium.Api tc.appID, tc.apiKey, apiVersion: '2'
      assert.equal api._apiVersion, '2'

  suite "bucket()", ->
    test "calling without a bucketName should throw an error", ->
      fn = =>
        api = new Simperium.Api tc.appID, tc.apiKey
        api.bucket()
      assert.throws fn, TypeError, /Missing bucketName/

