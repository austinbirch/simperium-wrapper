{tc, assert} = require './test_helpers'

nock = require 'nock'
Simperium = require '../lib/index'

suite "Bucket", ->
  setup ->
    @api = new Simperium.Api tc.appID, tc.apiKey
    @bucket = @api.bucket tc.bucketName

  suite "get()", ->
    test "throws an error if no id is passed", ->
      fn = =>
        @bucket.get()
      assert.throws fn, TypeError, /Missing itemID/

    test "returns the value of an existing item", (done) ->
      itemID = '1'
      itemMock =
        name: 'test'

      scope = nock('https://api.simperium.com')
        .get("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}")
        .reply(200, itemMock)

      cb = (err, item) =>
        assert.isNull err
        assert.equal scope.isDone(), true
        assert.equal item.name, itemMock.name
        done()
      @bucket.get itemID, cb

    test "returns a 404 error if the item does not exist", (done) ->
      itemID = '1'

      scope = nock('https://api.simperium.com')
        .get("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}")
        .reply(404)

      cb = (err, item) =>
        assert.instanceOf err, Error
        assert.match err.message, /404 Not Found/
        assert.isNull item
        done()
      @bucket.get itemID, cb

    test "returns Error if server replies with invalid JSON", (done) ->
      itemID = '1'
      invalidJSON = 'invalidJSON'

      scope = nock('https://api.simperium.com')
        .get("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}")
        .reply(200, invalidJSON)

      cb = (err, item) =>
        assert.instanceOf err, Error
        assert.match err.message, /Invalid JSON/
        assert.isNull item
        done()
      @bucket.get itemID, cb

  suite "new()", ->
    test "throws error if no data is passed", ->
      assert.throws @bucket.new, TypeError, /Missing data/

    test "calls back with the itemID of the new item", (done) ->
      itemID = '1'
      newItem =
        name: 'test'
        id: itemID

      scope = nock('https://api.simperium.com')
        .post("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}?response=1",
          newItem)
        .reply(200, newItem)

      @bucket.new newItem, (err, id) ->
        assert.isNull err
        assert.equal itemID, id
        assert.equal scope.isDone(), true
        done()

    test "calls back with data of the new item", (done) ->
      itemID = '1'
      newItem =
        name: 'test'
        id: itemID

      scope = nock('https://api.simperium.com')
        .post("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}?response=1",
          newItem)
        .reply(200, newItem)

      @bucket.new newItem, (err, id, data) ->
        assert.isNull err
        assert.equal itemID, id
        assert.equal data.id, itemID
        assert.equal scope.isDone(), true
        done()

    test "returns Error if server returns invalid JSON", (done) ->
      itemID = '1'
      newItem =
        name: 'test'
        id: itemID
      invalidJSON = 'invalidJSON'

      scope = nock('https://api.simperium.com')
        .post("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}?response=1",
          newItem)
        .reply(200, invalidJSON)

      @bucket.new newItem, (err, id) ->
        assert.instanceOf err, Error
        assert.match err.message, /Invalid JSON/
        assert.equal scope.isDone(), true
        done()

    test "returns 412 error if data is not different", (done) ->
      itemID = '1'
      newItem =
        name: 'test'
        id: itemID

      scope = nock('https://api.simperium.com')
        .post("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}?response=1",
          newItem)
        .reply(412)

      cb = (err, id) =>
        assert.isNull id
        assert.instanceOf err, Error
        assert.match err.message, /412 Precondition Failed/
        assert.equal scope.isDone(), true
        done()
      @bucket.new newItem, cb

  suite "set()", ->
    test "throws error if no itemID is passed", ->
      fn = =>
        @bucket.set()
      assert.throws fn, TypeError, /Missing itemID/

    test "throws error if no data is passed", ->
      fn = =>
        @bucket.set '1'
      assert.throws fn, TypeError, /Missing data/

    test "does not throw error upon success", (done) ->
      itemID = '1'
      change =
        name: 'mary'

      scope = nock('https://api.simperium.com')
        .post("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}?response=1",
          change)
        .reply(200)

      cb = (err) ->
        assert.isNull err
        assert.equal scope.isDone(), true
        done()
      @bucket.set itemID, change, cb

    test "returns data on success", (done) ->
      itemID = '1'
      change =
        name: 'mary'

      scope = nock('https://api.simperium.com')
        .post("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}?response=1",
          change)
        .reply(200, change)

      cb = (err, data) ->
        assert.isNull err
        assert.equal data.name, change.name
        assert.equal scope.isDone(), true
        done()
      @bucket.set itemID, change, cb

    test "returns 412 error if data is not different", (done) ->
      itemID = '1'
      change =
        name: 'mary'

      scope = nock('https://api.simperium.com')
        .post("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}?response=1",
          change)
        .reply(412)

      cb = (err) =>
        assert.instanceOf err, Error
        assert.match err.message, /412 Precondition Failed/
        assert.equal scope.isDone(), true
        done()
      @bucket.set itemID, change, cb

  suite "delete()", ->
    test "throws an error if no itemID is passed", ->
      fn = =>
        @bucket.delete()
      assert.throws fn, TypeError, /Missing itemID/

    test "does not throw an error on success", (done) ->
      itemID = '1'

      scope = nock('https://api.simperium.com')
        .delete("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}")
        .reply(200)

      cb = (err) =>
        assert.isNull err
        assert.equal scope.isDone(), true
        done()
      @bucket.delete itemID, cb

    test "returns 404 error if data did not exist", (done) ->
      itemID = '1'

      scope = nock('https://api.simperium.com')
        .delete("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/i/#{itemID}")
        .reply(404)

      cb = (err) =>
        assert.instanceOf err, Error
        assert.match err.message, /404 Not Found/
        assert.equal scope.isDone(), true
        done()
      @bucket.delete itemID, cb


  suite "all()", ->
    test "returns all of the changes", (done) ->
      mockChanges =
        'change_1': {
          id: '1'
        }
        'change_2': {
          id: '2'
        }

      scope = nock('https://api.simperium.com')
        .get("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/all")
        .reply(200, mockChanges)

      cb = (err, changes) =>
        assert.equal changes['change_1'].id, mockChanges['change_1'].id
        assert.equal scope.isDone(), true
        done()

      @bucket.all cb

    test "uses the changeversion passed to all in the API call", (done) ->
      cv = '1'
      mockChanges =
        'change_2': {
          id: '2'
        }

      scope = nock('https://api.simperium.com')
        .get("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/all?cv=#{cv}")
        .reply(200, mockChanges)

      cb = (err, changes) =>
        assert.isUndefined changes['change_1']
        assert.equal changes['change_2'].id, mockChanges['change_2'].id
        assert.equal scope.isDone(), true
        done()
      @bucket.all { cv: cv }, cb

    test "uses the data parameter in the query if passed", (done) ->
      data = true

      mockChanges =
        'change_1': {
          id: '1'
        }

      scope = nock('https://api.simperium.com')
        .get("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/all?data=#{data}")
        .reply(200, mockChanges)

      cb = (err, changes) =>
        assert.equal changes['change_1'].id, mockChanges['change_1'].id
        assert.equal scope.isDone(), true
        done()
      @bucket.all { data: data }, cb

    test "uses the username parameter in the query if passed", (done) ->
      username = true

      mockChanges =
        'change_1': {
          id: '1'
        }

      scope = nock('https://api.simperium.com')
        .get("/#{tc.apiVersion}/#{tc.appID}/#{tc.bucketName}/all?username=#{username}")
        .reply(200, mockChanges)

      cb = (err, changes) =>
        assert.equal changes['change_1'].username, mockChanges['change_1'].username
        assert.equal scope.isDone(), true
        done()
      @bucket.all { username: username }, cb

